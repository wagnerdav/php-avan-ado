<?php
/*
@Author: Wagner Viana
@Date:18/11/2014
@Function: Setar rotas iniciais
*/
	//spl_autoload_extensions('.php');//13
	//spl_autoload_register();
	
	//require_once('config.php');
	
	//pega todos arquivos dentro da controller e da autoload
	spl_autoload_register(function ($class) {
		require_once(str_replace('\\','/', $class . '.php'));	
	});

	$config = new config();

?>