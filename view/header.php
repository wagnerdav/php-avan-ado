<?php
/*
@Author: Wagner Viana
@Data: 18/11/2014
@Function: 
*/
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo $this->titulo; ?></title>
			<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl.'/public/bootstrap/dist/css/bootstrap-responsive.min.css'; ?>"/>
			<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl.'/public/bootstrap/dist/css/bootstrap.min.css'; ?>"/>
			<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl.'/public/css/style.css'; ?>"/>
			<script type="text/javascript" src="<?php echo $this->baseUrl.'/public/js/script.js'; ?>"></script>
			<script type="text/javascript" src="<?php echo $this->baseUrl.'/public/js/bootstrap.min.js'; ?>"></script>
			<script type="text/javascript" src="<?php echo $this->baseUrl.'/public/js/function.js'; ?>"></script>	

	</head>
	<body>
		
